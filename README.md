# webbrowser.html

uma obra de netart sobre o passado, o presente e o futuro da World Wide Web

## repositório (privado)

código-fonte, imagens e documentos disponíveis no gitlab:

* https://gitlab.com/joenio/netart-webbrowser.html

## ambiente de producao publico

* http://webbrowser.html.4two.art

## atualizar README.md.html

```sh
make README.md.html
```

## autor

* Joenio Marques da Costa <joenio@4two.art>

[![Licença Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)
<br/>
Esta obra está licenciada com uma Licença
[Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/)
